function tableFromJson() {
    var pythonCourse = [
        {'Course ID': '1', 'Course Name': '2022 Complete Python Bootcamp From Zero to Hero in Python',
            'Description': 'Learn Python like a Professional Start from the basics and go all the way to creating your own applications and games.',
            'Created By': 'Jose Portilla', 'Price': '455', 'Rating': '4.6'
        },
        {'Course ID': '2', 'Course Name': 'Machine Learning A-Z™: Hands-On Python & R In Data Science',
            'Description':'Learn to create Machine Learning Algorithms in Python and R from two Data Science experts. Code templates included.',
            'Created By': 'Ligency I Team', 'Price': '455', 'Rating':'4.5'
        },
        {'Course ID': '3', 'Course Name': 'Python for Data Science and Machine Learning Bootcamp',
        'Description': 'Learn how to use NumPy, Pandas, Seaborn , Matplotlib , Plotly , Scikit-Learn , Machine Learning, Tensorflow , and more!',
            'Created By': 'Jose Portilla', 'Price': '455', 'Rating':'4.7'
        },
        {'Course ID': '4', 'Course Name': '100 Days of Code: The Complete Python Pro Bootcamp for 2022',
            'Description':'Master Python by building 100 projects in 100 days. Learn data science, automation, build websites, games and apps!',
            'Created By': 'Dr. Angela Yu', 'Price': '455', 'Rating':'4.7'
        },
        {'Course ID': '5', 'Course Name': 'Learn Python Programming Masterclass',
            'Description': 'This Python For Beginners Course Teaches You The Python Language Fast. Includes Python Online Training With Python 3.',
            'Created By': 'Tim Buchalka\'s Learn Programming Academy', 'Price': '455', 'Rating':'4.6'
        },
        {'Course ID': '6', 'Course Name': 'The Python Mega Course 2022: Build 10 Real-World Programs',
            'Description':'Go from Python absolute beginner to building advanced Python programs! Learn Django, Flask, GUI, web scraping, & more!',
            'Created By': 'Ardit Sulce', 'Price': '455', 'Rating' : '4.6'
        }
    ]


    var col = [];
    for (var i = 0; i < pythonCourse.length; i++) {
        for (var key in pythonCourse[i]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
            }
        }
    }
 
    var table = document.createElement("table");

    var tr = table.insertRow(-1);                 

    for (var i = 0; i < col.length; i++) {
        var th = document.createElement("th");      
        th.innerHTML = col[i];
        tr.appendChild(th);
    }

    for (var i = 0; i < pythonCourse.length; i++) {

        tr = table.insertRow(-1);

        for (var j = 0; j < col.length; j++) {
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = pythonCourse[i][col[j]];
        }
    }

    var divShowData = document.getElementById('showData');
    divShowData.innerHTML = "";
    divShowData.appendChild(table);
}